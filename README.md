# **Jesse's Dotfiles**
Because every developer needs them!

> ## **Installing Automatically:**
> ```sh
> curl -Lks https://bit.ly/3nDfDya | /bin/bash
> ```

> ## **Installing Manually:**
> ```sh
> git clone --separate-git-dir=~/.dotfiles git@gitlab.com:jeg6187/dotfiles.git ~
> alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
> dotfiles checkout
> dotfiles config status.showUntrackedFiles no
> dotfiles submodule update --init --recursive
> ```
