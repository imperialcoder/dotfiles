# system
alias shutdown="sudo shutdown now"
alias restart="sudo reboot"
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
alias dot="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
alias ls="lsd --group-dirs first"

# editors
alias doom="$HOME/.emacs.d/bin/doom"
alias emacs="emacsclient -fs -c -a 'emacs'"
alias code="vscodium"
alias v="nvim"
alias mdt='mdt --dir ~/tasks --inbox ~/documents/tasks/"$(date -I).md"'
alias cron="env VISUAL=\"$EDITOR\" crontab -e"
alias scron='sudo env VISUAL=\"nvim\" crontab -u root -e'

# portage
alias emerge="sudo emerge"
alias dispatch-conf="sudo dispatch-conf"
alias eselect="sudo eselect"
alias emaint="sudo emaint"
alias eix="sudo eix"
alias eix-update="sudo eix-update"
alias genkernel="sudo genkernel"

#xbps
alias xi="sudo xbps-install"
alias xr="sudo xbps-remove"
alias xq="xbps-query"

# XDG 
alias o="xdg-open"
alias d=default

# programs
alias firefoxp="firefox --no-remote -P"
alias tt="tt -notheme"
alias monerod="monerod --data-dir "$XDG_DATA_HOME"/bitmonero"
alias wget="wget --hsts-file=$XDG_DATA_HOME/wget-hsts"
alias yarn="yarn --use-yarnrc $XDG_CONFIG_HOME/yarn/config"
alias nvidia-settings="nvidia-settings --config=$XDG_CONFIG_HOME/nvidia/settings"
alias newsboat="newsboat --quiet -r"
alias dac="dotnet-aspnet-codegenerator"
alias ufw="sudo ufw"
alias flatpak="sudo flatpak"
alias adb='HOME="$XDG_DATA_HOME"/android adb'
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias yarn=yarn --use-yarnrc $XDG_CONFIG_HOME/yarn/config

