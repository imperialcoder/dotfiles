# just so we can test the speed of our config
zmodload zsh/zprof

# source aliases
source $ZDOTDIR/aliases.zsh

# source functions
# source $HOME/.config/functions/functions.sh

# keybindings
bindkey -v
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "\e[3~" delete-char

# tmux workflow keybindings
bindkey -s ^f "tmux-sessionizer\n" bindkey -s ^a "tsesh\n"
bindkey -s ^k "tmux kill-server\n"

# zellij workflow keybindings
#bindkey -s ^f "zellij-sessionizer\n"
#bindkey -s ^a "zellij-sessionizer SYSTEM\n"
#bindkey -s ^k "zellij kill-all-sessions\n"

# open up lf
bindkey -s ^o "lf\n"

function zenv {
	eval $EDITOR $HOME/.zshenv
}

function zconfig {
	eval $EDITOR $ZDOTDIR/.zshrc
}

function zprofile {
	eval $EDITOR $ZDOTDIR/.zprofile
}

function zaliases {
	eval $EDITOR $HOME/.config/aliases/aliases
}

function zreload {
    source $HOME/.zshenv
	source $ZDOTDIR/.zshrc
	echo "zsh reloaded!"
}

function zspeed {
	zprof | less
}

function update {
	# arch updates
    if [ -x "$(command -v /usr/bin/aura)" ]; then
	    yes | sudo aura -Syu
	    yes | sudo aura -Au
    elif [ -x "$(command -v /usr/bin/pacman)" ]; then
        yes | sudo pacman -Syu
    fi

    # gentoo updates
    if [ -x "$(command -v /usr/bin/emerge)" ]; then
        sudo emerge --sync
        sudo emerge -uDNrv @world
        sudo emerge -c
    fi

    # submodule updates
    dotfiles submodule update --recursive --remote
    make -C $HOME/git/bin/

    # flatpak updates
    if [ -x "$(command -v flatpak)" ]; then
        sudo flatpak update
    fi

	printf "Would you like to reboot? (y/n): "
	read REBOOT
	if [ "$REBOOT" = y ]; then
	   sudo reboot
	fi
}

function tsesh {
    if [ -x "$(command -v tmuxinator)" ]; then
        tmux a || tmuxinator start system
    else
        tmux a || tmux new -s SYSTEM
    fi
}

function seshlog {
    if [ $USE_XORG = true ]; then 
        $EDITOR $XDG_DATA_HOME/sddm/xorg-session.log
    else 
        $EDITOR $XDG_DATA_HOME/sddm/wayland-session.log
    fi
}

# completions
autoload -Uz compinit
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
	compinit
else
	compinit -C
fi
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path $ZDOTDIR/.zcompcache

# plugins
source $PLUGINS/zsh-autosuggestions/zsh-autosuggestions.zsh
source $PLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fpath=($PLUGINS/zsh-completions/src $fpath)

# fuck 
[[ -x "$(command -v thefuck)" ]] && \
    eval $(thefuck --alias)

# zoxide
[[ -x "$(command -v zoxide)" ]] && \
    eval "$(zoxide init zsh)"

# prompt
[[ -x "$(command -v starship)" ]] && \
    eval "$(starship init zsh)"
