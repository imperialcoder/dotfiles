#!/bin/bash
[ -f "$HOME/.zshenv" ] && . "$HOME/.zshenv"


if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ] && [ "$(fgconsole)" -eq 1 ]; then

    if [ "$USE_XORG" = true ]; then
        exec startx 2>&1 | tee ~/.local/share/xorg.log
    else
        exec dbus-run-session $WAYLAND_WM 2>&1 | tee ~/.local/share/wayland.log
    fi
fi
