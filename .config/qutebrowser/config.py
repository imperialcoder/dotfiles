import os

from qutebrowser.config.config import ConfigContainer  # noqa: F401
# pylint: disable=C0111
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401

config: ConfigAPI = config  # noqa: F821 pylint: disable=E0602,C0103
c: ConfigContainer = c  # noqa: F821 pylint: disable=E0602,C0103

config_dir = os.environ["XDG_CONFIG_HOME"] + "/qutebrowser"
script_dir = config_dir + "/userscripts"

# ----- theme and colors -----
config.source("themes/qute-city-lights/city-lights-theme.py")
c.fonts.default_size = "10.5pt"
c.colors.webpage.bg = "#1D252C"
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.preferred_color_scheme = "dark"

# fonts
c.fonts.default_family = "Hack Nerd Font Mono"
c.fonts.web.family.standard = "OverpassM Nerd Font Mono"

# keep config set from UI
config.load_autoconfig(False)

# ----- keybindings -----
config.bind(
    "R",
    'config-source ;; greasemonkey-reload --quiet ;; message-info "config and scripts reloaded!"',
)
config.bind("M", "hint -a links spawn linkhandler {hint-url}")
config.bind(";m", "spawn linkhandler {url}")
config.bind("Aa", "spawn mpv --no-video {url}")
config.bind("A", "hint -a links spawn mpv --no-video {hint-url}")
config.bind("Z", "spawn foot -a YTDLP yt-dlp -P ~/videos/yt-dlp {url}")
config.bind("X", f"spawn --userscript {script_dir}/qute-bitwarden")
config.bind("I", "cmd-set-text -s :open -p ")
config.bind("<Ctrl-s>", "cmd-set-text -s :session-save")
config.bind("<Ctrl-k>", "cmd-set-text -s :open -w ")
config.bind("W", "spawn --userscript whitelist add {url} ;; adblock-update ;; reload")

# faster key scrolling
config.bind("h", "cmd-run-with-count 2 scroll left")
config.bind("j", "cmd-run-with-count 2 scroll down")
config.bind("k", "cmd-run-with-count 2 scroll up")
config.bind("l", "cmd-run-with-count 2 scroll right")

config.bind("left", "cmd-run-with-count 2 scroll left")
config.bind("down", "cmd-run-with-count 2 scroll down")
config.bind("up", "cmd-run-with-count 2 scroll up")
config.bind("right", "cmd-run-with-count 2 scroll right")


# opening in another browser
config.bind("<Ctrl-p>", "spawn firefox --private-window {url}")
config.bind("<Ctrl-Shift-p>", "spawn firefox --private-window")

config.bind("<Ctrl-f>", "spawn firefox --new-window {url}")
config.bind("<Ctrl-Shift-f>", "spawn firefox --new-window")

config.bind("<Ctrl-u>", "spawn chromium --new-window {url}")
config.bind("<Ctrl-Shift-u>", "spawn chromium --new-window")

config.bind("<Ctrl-i>", "spawn chromium --incognito {url}")
config.bind("<Ctrl-Shift-i>", "spawn chromium --incognito")

# session keybindings
config.bind("ww", "session-save")
config.bind("wq", "session-save ;; close")
config.bind(";w", "cmd-set-text -s :session-save ")
config.bind(";;", "session-save ;; cmd-set-text -s :session-load --clear ")
config.bind(";s", "session-save ;; cmd-set-text -s :session-load ")

config.bind("xs", "config-cycle statusbar.show always never")
config.bind("xt", "config-cycle tabs.show always never")
config.bind(
    "xx",
    "config-cycle tabs.show always never ;; config-cycle statusbar.show always never",
)

# ----- some privacy and security stuff -----
c.content.blocking.enabled = True
c.content.blocking.method = "both"
# c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.3'
c.content.headers.accept_language = "en-US,en;q=0.5"

# I wish I could get this thing to work :(
# c.content.headers.custom['accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}'

with open(config_dir + "/whitelist") as whitelist:
    c.content.blocking.whitelist = [line.rstrip() for line in whitelist]

c.content.cookies.accept = "no-3rdparty"
c.content.webrtc_ip_handling_policy = "default-public-interface-only"
c.content.autoplay = False

# ----- start pages and search engines -----
c.url.start_pages = ["about:blank"]
c.url.default_page = "about:blank"
c.url.searchengines = {
    "DEFAULT": "http://localhost/search?q={}",
    "!ytp": "https://piped.video/results?search_query={}",
    "!ytc": "https://youtube.com/results?search_query={}",
    "!ody": "https://odysee.com/$/search?q={}",
    "!amz": "https://amazon.com/s?k={}",
    "!alt": "https://alternativeto.net/browse/search/?q={}",
    "!ggl": "https://google.com/search?q={}",
    "!ddg": "https://duckduckgo.com/?q={}",
    "!grf": "https://greasyfork.org/en/scripts?q={}",
    "!goz": "https://gpo.zugaina.org/Search?search={}",
    "!urd": "https://www.urbandictionary.com/define.php?term={}",
    "!red": "https://www.reddit.com/search/?q={}",
    "!lem": "https://www.search-lemmy.com/results?query={}&page=1&mode=posts",
    "!gmp": "https://www.google.com/maps/search/{}",
    "!omp": "https://www.openstreetmap.org/search?query={}#map=5/40.145/-98.745",
    "!twt": "https://www.twitch.tv/search?term={}",
    "!hel": "https://artifacthub.io/packages/search?ts_query_web={}&sort=relevance&page=1",
    "!doh": "https://hub.docker.com/search?q={}",
    "!spo": "https://open.spotify.com/search/{}",
    "!cro": "https://www.crunchyroll.com/search?q={}"
}

# ---- quality of life stuff -----
c.scrolling.bar = "never"
c.scrolling.smooth = True
c.downloads.remove_finished = 2000
c.content.local_content_can_access_remote_urls = True
c.session.lazy_restore = True
c.tabs.mode_on_change = "restore"
c.auto_save.session = True
c.content.notifications.enabled = False
c.qt.highdpi = True
c.content.pdfjs = True

# ---- terminal file picker -----
c.fileselect.handler = "external"
if c.fileselect.handler == "external":
    prefix: list[str] = []
    terminal: str = os.environ["TERMINAL"]
    identifier = "FILE_SELECTOR"
    suffix = ["lf", "-selection-path", "{}"]
    match terminal:
        case "wezterm":
            prefix.extend([terminal, "start", "--class", identifier])
        case "foot" | "footclient":
            prefix.extend([terminal, "-a", identifier])
        case "alacritty" | "kitty":
            prefix.extend([terminal, "--class", identifier])
        case _:
            prefix.extend([terminal, *suffix])
    c.fileselect.single_file.command = prefix + suffix
    c.fileselect.multiple_files.command = prefix + suffix
