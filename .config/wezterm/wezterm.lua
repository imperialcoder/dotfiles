local wezterm = require("wezterm")
local config = wezterm.config_builder()
local home = os.getenv("HOME")
local color_scheme = os.getenv("COLOR_SCHEME")

wezterm.add_to_config_reload_watch_list(home .. "/.config/wezterm/colors")

config.font_size = 11
config.font = wezterm.font("Hack Nerd Font Mono")
config.use_resize_increments = true
config.window_decorations = "RESIZE"
config.adjust_window_size_when_changing_font_size = true
config.audible_bell = "Disabled"

config.window_background_opacity = 0.8
config.enable_tab_bar = false

--config.background = {
--	{
--		source = { Color = "black" },
--		opacity = 0.8,
--		width = "100%",
--		height = "100%",
--	},
--}

local function switch(type)
	local cases = {
		["wallust"] = function()
			config.color_scheme = "wallust"
		end,
		["tokiyonight"] = function()
			config.color_scheme = "tokiyonight"
		end,
		["cyberdream"] = function()
			config.colors = require("colors/cyberdream")
		end,
		["everforest"] = function()
			config.color_scheme = "Everforest Dark (Hard)"
		end,
	}

	local case = cases[type] or cases["everforest"]
	case()
end

switch(color_scheme)

config.macos_window_background_blur = 20

config.window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
}

config.keys = {
	{
		key = "Enter",
		mods = "CTRL",
		action = wezterm.action.ActivateCopyMode,
	},
}

config.bypass_mouse_reporting_modifiers = "CTRL"
config.mouse_bindings = {
	{
		event = { Up = { streak = 1, button = "Left" } },
		mods = "CTRL",
		action = wezterm.action.OpenLinkAtMouseCursor,
	},
	{
		event = { Up = { streak = 1, button = "Left" } },
		mods = "SHIFT",
		action = wezterm.action.OpenLinkAtMouseCursor,
	},
}

local center_content = function(window, pane)
	local win_dim = window:get_dimensions()
	local tab_dim = pane:tab():get_size()
	local overrides = window:get_config_overrides() or {}
	local padding_top = (win_dim.pixel_height - tab_dim.pixel_height) / 2
	local new_padding = {
		left = 0,
		right = 0,
		top = padding_top,
		bottom = 0,
	}
	overrides.window_padding = new_padding
	window:set_config_overrides(overrides)
end

wezterm.on("window-resized", center_content)
wezterm.on("window-config-reloaded", center_content)

return config
