-- getting/setting the token
local token_loc = os.getenv("XDG_CACHE_HOME") .. "/lichess_api_token"
local lichess_auth_url =
	"https://lichess.org/account/oauth/token/create?scopes[]=challenge:write&scopes[]=board:play&description=vim+lichess"

function set_token()
	local lines = {}
	for line in io.lines(token_loc) do
		lines[#lines] = line
	end
	vim.g.lichess_api_token = lines[0]
end

if io.open(token_loc, "rb") ~= nil then
	set_token()
else
	local handle = io.popen("uname")
	if handle then
		local result = handle:read("*a")
		handle:close()

		if result:match("Linux") then
			os.execute('xdg-open "' .. lichess_auth_url .. '" &> /dev/null')
		elseif result:match("Darwin") then
			os.execute('open "' .. lichess_auth_url .. '" &> /dev/null')
		end
	end

	vim.ui.input({ prompt = "enter lichess api token: " }, function(input)
		if input ~= nil then
			file = io.open(token_loc, "w")
			file:write(input)
			file:close()
		end
	end)
	set_token()
end

-- lichess settings
vim.g.lichess_time = 10
vim.g.lichess_rated = 0

-- keymappings
vim.keymap.set("n", "<leader>lm", ":LichessMakeMoveUCI")
vim.keymap.set("n", "<leader>lc", ":LichessChat")
vim.keymap.set("n", "<leader>la", ":LichessAbort")
vim.keymap.set("n", "<leader>lr", ":LichessResign")
vim.keymap.set("n", "<leader>ldo", ":LichessOfferDraw")
vim.keymap.set("n", "<leader>lda", ":LichessAcceptDraw")
vim.keymap.set("n", "<leader>ldd", ":LichessDeclineDraw")
vim.keymap.set("n", "<leader>lm", ":LichessMakeMoveUCI")
