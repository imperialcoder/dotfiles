-- WIP replacing null-ls
-- https://github.com/stevearc/conform.nvim
--

require("mason-tool-installer").setup({
	ensure_installed = {
		"stylua",
		"prettierd",
		"yamlfix",
		"fixjson",
		"mdformat",
		"cbfmt",
		"stylelint",
	},
})

require("conform").setup({
	formatters_by_ft = {
		lua = { "stylua" },
		python = { "ruff_fix", "ruff_format", "ruff_organize_imports" },
		javascript = { "prettierd" },
		vue = { "prettierd" },
		tf = { "terraform_fmt" },
		yaml = { "yamlfix" },
		json = { "fixjson" },
		markdown = { "mdformat" },
	},
	format_on_save = function(bufnr)
		-- Disable with a global or buffer-local variable
		if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
			return
		end
		return { timeout_ms = 500, lsp_fallback = true }
	end,
})

require("conform").formatters.yamlfix = {
	env = {
		YAMLFIX_WHITELINES = "1",
		YAMLFIX_SECTION_WHITELINES = "1",
		YAMLFIX_SEQUENCE_STYLE = "block_style",
	},
}

vim.api.nvim_create_user_command("FormatToggle", function(args)
	if args.bang then
		vim.b.disable_autoformat = not vim.b.disable_autoformat
	else
		vim.g.disable_autoformat = not vim.g.disable_autoformat
	end
end, {
	desc = "Toggles autoformat-on-save",
	bang = true,
})

vim.api.nvim_create_user_command("FormatDisable", function(args)
	if args.bang then
		-- FormatDisable! will disable formatting just for this buffer
		vim.b.disable_autoformat = true
	else
		vim.g.disable_autoformat = true
	end
end, {
	desc = "Disable autoformat-on-save",
	bang = true,
})

vim.api.nvim_create_user_command("FormatEnable", function()
	vim.b.disable_autoformat = false
	vim.g.disable_autoformat = false
end, {
	desc = "Re-enable autoformat-on-save",
})
