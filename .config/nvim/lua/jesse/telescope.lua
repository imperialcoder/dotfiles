local builtin = require("telescope.builtin")
local telescope = require("telescope")

vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})

telescope.setup({
	defaults = {
		file_ignore_patterns = {
			".git/",
			"node_modules/",
			"dist",
			"build",
			"venv",
		},
	},
	extensions = {
		file_browser = {
			hijack_netrw = false,
			depth = 2,
		},
	},
	pickers = {
		find_files = {
			hidden = true,
		},
	},
})

telescope.load_extension("file_browser")
telescope.load_extension("live_grep_args")

vim.keymap.set("n", "<leader>fb", function()
	telescope.extensions.file_browser.file_browser()
end)

vim.keymap.set("n", "<leader>fg", function()
	telescope.extensions.live_grep_args.live_grep_args()
end)
