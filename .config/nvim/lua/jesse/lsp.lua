local servers = {
	"ansiblels",
	"bashls",
	"rust_analyzer",
	"lua_ls",
	"ts_ls",
	"cssls",
	"html",
	"dockerls",
	"yamlls",
	"marksman",
	"ansiblels",
	"gopls",
	"taplo",
	"helm_ls",
	"volar",
	"eslint",
	"terraformls",
	"stylelint_lsp",
	"typos_lsp",
	"pyright",
	--"csharp_ls",
}

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

local opts = { noremap = true, silent = true }
vim.keymap.set("n", "<space>e", vim.diagnostic.open_float, opts)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
vim.keymap.set("n", "<space>q", vim.diagnostic.setloclist, opts)

local on_attach = function(client, bufnr)
	--vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	local bufopts = { noremap = true, silent = true, buffer = bufnr }
	vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
	vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
	vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
	vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
	vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
	vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, bufopts)
	vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
	vim.keymap.set("n", "<space>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, bufopts)
	vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, bufopts)
	vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, bufopts)
	vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, bufopts)
	vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
	vim.keymap.set("n", "<space>cf", vim.lsp.buf.format, bufopts)
end

require("mason").setup()
require("mason-lspconfig").setup({
	ensure_installed = servers,
})

local lspconfig = require("lspconfig")

require("mason-lspconfig").setup_handlers({
	-- Will be called for each installed server that doesn't have
	-- a dedicated handler.
	function(server_name) -- default handler (optional)
		lspconfig[server_name].setup({
			on_attach = on_attach,
			capabilities = capabilities,
			settings = {
				Lua = {
					diagnostics = {
						globals = { "vim" },
						disable = { "lowercase-global" },
					},
				},
				css = {
					validate = false,
				},
				yaml = {
					keyOrdering = false,
				},
				python = {
					venvPath = { "venv/*", ".venv/*" },
					analysis = {
						autoSearchPaths = true,
						diagnosticMode = "openFilesOnly",
						useLibraryCodeForTypes = true,
					},
				},
				eslint = {
					rules = {
						customizations = {
							-- { rule = "vue/multi-word-component", severity = "off" },
						},
					},
				},
				-- stylelintplus = {
				-- 	autoFixOnSave = true,
				-- },
			},
		})
	end,
})

-- handler for volar
lspconfig.volar.setup({
	on_attach = on_attach,
	capabilities = capabilities,
	filetypes = { "vue", "javascript", "typescript", "javascriptreact", "typescriptreact" },
	init_options = {
		vue = {
			hybridMode = false,
		},
		typescript = {
			tsdk = vim.fn.getcwd() .. "node_modules/typescript/lib",
		},
	},
})

-- handler for pyright
lspconfig.pyright.setup({
	settings = {
		python = {
			analysis = {
				typeCheckingMode = "basic", -- i use ruff to check types
				useLibraryCodeForTypes = true,
			},
		},
	},
})
