-- WIP replacing null_ls
-- https://github.com/mfussenegger/nvim-lint

require("mason").setup()

-- Configure the linters you want to run per file type. For example:
require("lint").linters_by_ft = {
	markdown = { "markdownlint", "vale" },
	python = { "ruff" },
	javascript = { "eslint_d" },
	typescript = { "eslint_d" },
	vue = { "eslint_d" },
	yaml = { "yamllint" },
	terraform = { "tflint" },
	--["yaml.ansible"] = { "ansible_lint" },
}

require("lint").linters.stylelint = {}

require("mason-nvim-lint").setup({
	automatic_setup = true,
	ensure_installed = {},
})

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
	callback = function()
		require("lint").try_lint()
	end,
})
