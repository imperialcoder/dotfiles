local nnoremap = require("jesse.keymap").nnoremap

nnoremap("<leader>o", "<cmd>Lf<CR>")

nnoremap("<leader>lr", "<cmd>LspRestart<CR>")

nnoremap("<leader>fmt", "<cmd>FormatToggle!<CR>")
nnoremap("<leader>fme", "<cmd>FormatEnable<CR>")
nnoremap("<leader>fmd", "<cmd>FormatDisable<CR>")

nnoremap("<C-n>", "<cmd>tabn<CR>")
nnoremap("<C-p>", "<cmd>tabp<CR>")
nnoremap("<C-t>", "<cmd>tabnew<CR>")
nnoremap("<C-x>", "<cmd>tabc<CR>")
