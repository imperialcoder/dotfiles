require("jesse.set")
require("jesse.remap")
require("jesse.providers")
require("jesse.packer")
require("jesse.telescope")
require("jesse.harpoon")
require("jesse.lualine")

require("jesse.lsp")
require("jesse.conform")
require("jesse.nvim-lint")

require("jesse.treesitter")
require("jesse.cmp")

require("nvim-highlight-colors").setup({})

require("jesse.lf")
require("jesse.toggle_term")
require("jesse.transparent")

-- just for fun :)
require("jesse.lichess")
