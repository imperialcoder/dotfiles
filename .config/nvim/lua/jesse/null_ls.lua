local mason = require("mason")
local null_ls = require("null-ls")
local mason_null_ls = require("mason-null-ls")

mason.setup()

mason_null_ls.setup({
	automatic_setup = true,
	ensure_installed = {
		"ansible-lint",
		"stylua",
		"eslint_d",
		"prettier",
		"spell",
		"hadolint",
		"jq",
		"ltrs",
	},
})

null_ls.setup({
    sources = {
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.diagnostics.eslint,
        null_ls.builtins.completion.spell,
        null_ls.builtins.formatting.prettier.with({
            extra_filetypes = { "typescriptreact", "javascriptreact" },
        }),
    }
})
