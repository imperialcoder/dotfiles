require("nvim-treesitter.configs").setup({
	ensure_installed = { "lua", "rust", "html", "css", "javascript", "typescript" },
	sync_install = false,
	auto_install = true,
	-- ignore_install = { "javascript" },

	highlight = {
		enable = true,
		-- disable = { "c", "rust" },
		additional_vim_regex_highlighting = false,
	},

	indent = {
		enable = true,
		disable = { "python" },
	},
})

require("tree-sitter-just").setup({})

vim.filetype.add({
	extension = {
		gotmpl = "gotmpl",
	},
	pattern = {
		[".*/templates/.*%.tpl"] = "helm",
		[".*/templates/.*%.ya?ml"] = "helm",
		["helmfile.*%.ya?ml"] = "helm",
	},
})
