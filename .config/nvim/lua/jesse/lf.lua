vim.g.lf_netrw = 1

require("lf").setup({
	default_actions = {
		["<C-t>"] = "tabedit",
		["<C-x>"] = "split",
		["<C-v>"] = "vsplit",
		["<C-o>"] = "tab drop",
	},
	winblend = 0,
	border = "rounded",
	direction = "float",
	escape_quit = true,
	highlights = {
		Normal = { guibg = "NONE" },
	},
})
