local venv = os.getenv("XDG_DATA_HOME") .. "/venv"
if io.open(venv .. "/bin/neovim") == nil then
	os.execute(venv .. "/bin/pip install neovim &> /dev/null")
end
vim.g.python_cmd = venv .. "/bin/python3"
vim.g.python3_host_prog = venv .. "/bin/python3"
