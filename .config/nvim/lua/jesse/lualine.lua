require("lualine").setup({
	sections = {
		lualine_x = { { "copilot", show_colors = true }, "encoding", "fileformat", "filetype" },
	},
	options = {
		theme = "auto",
	},
})
