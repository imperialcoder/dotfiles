local fn = vim.fn

-- automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	packer_bootstrap =
		fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
	vim.cmd([[packadd packer.nvim]])
end

-- autocommand to reload nvim on save
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost packer.lua source <afile> | PackerSync
  augroup end
]])

return require("packer").startup({
	function(use)
		use("wbthomason/packer.nvim")

		-- themes
		use("folke/tokyonight.nvim")
		use("scottmckendry/cyberdream.nvim")
		use("sainnhe/everforest")

		use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })

		use({
			"lmburns/lf.nvim",
			"nvim-lua/plenary.nvim",
			"akinsho/toggleterm.nvim",
		})

		-- cmp plugins
		use("hrsh7th/nvim-cmp")
		use("hrsh7th/cmp-buffer")
		use("hrsh7th/cmp-path")
		use("hrsh7th/cmp-cmdline")
		use("hrsh7th/cmp-nvim-lsp")
		use("onsails/lspkind.nvim")

		-- github copilot
		-- WIP I want to break this out but for some reason I'm getting an error with it
		-- use({
		-- 	"zbirenbaum/copilot.lua",
		-- 	cmd = "Copilot",
		-- 	event = "InsertEnter",
		-- 	config = function()
		-- 		require("copilot").setup({
		-- 			suggestion = { enabled = false },
		-- 			panel = { enabled = false },
		-- 			auto_refresh = true,
		-- 		})
		-- 	end,
		-- })
		-- use({
		-- 	"zbirenbaum/copilot-cmp",
		-- 	after = { "copilot.lua" },
		-- })
		use("AndreM222/copilot-lualine")

		-- snippet plugins
		use("L3MON4D3/LuaSnip")
		use("rafamadriz/friendly-snippets")
		use("saadparwaiz1/cmp_luasnip")

		-- lsp plugins
		use({
			"williamboman/mason.nvim",
			"williamboman/mason-lspconfig.nvim",
			"neovim/nvim-lspconfig",
			"WhoIsSethDaniel/mason-tool-installer.nvim",
		})

		-- linter plugins
		use({
			"stevearc/conform.nvim",
			"mfussenegger/nvim-lint",
			"rshkarin/mason-nvim-lint",
		})

		-- navigation plugins
		use({
			"nvim-telescope/telescope-file-browser.nvim",
			requires = {
				"nvim-telescope/telescope.nvim",
				"nvim-lua/plenary.nvim",
				"nvim-telescope/telescope-live-grep-args.nvim",
			},
		})

		use({
			"ThePrimeagen/harpoon",
			branch = "harpoon2",
			requires = { { "nvim-lua/plenary.nvim" } },
		})

		-- syntax plugins
		use("elkowar/yuck.vim")
		-- use({
		-- 	--"eraserhd/parinfer-rust",
		-- 	cmd = "ParinferOn",
		-- 	run = "cargo build --release",
		-- })
		use("ron-rs/ron.vim")
		use("kovetskiy/sxhkd-vim")

		-- misc plugins
		use("brenoprata10/nvim-highlight-colors")
		use("ThePrimeagen/vim-be-good")
		use("IndianBoy42/tree-sitter-just")
		use("waycrate/swhkd-vim")

		-- theming
		use({ "AlphaTechnolog/pywal.nvim", as = "pywal" })
		use({ "xiyaowong/transparent.nvim", as = "transparent" })
		use({
			"nvim-lualine/lualine.nvim",
			requires = { "nvim-tree/nvim-web-devicons", opt = true },
		})
		use({
			"nvim-tree/nvim-web-devicons",
			config = function()
				require("nvim-web-devicons").setup()
			end,
		})

		-- just for  fun
		-- use("luk400/vim-lichess")

		if packer_bootstrap then
			require("packer").sync()
		end
	end,
	config = {
		display = {
			open_fn = require("packer.util").float,
		},
	},
})
