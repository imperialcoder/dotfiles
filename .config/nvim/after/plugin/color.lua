local color_scheme = os.getenv("COLOR_SCHEME")

-- TOOOOKIYYYYYOOOOOOO
local function tokiyonight()
	require("tokyonight").setup({
		style = "night",
		transparent = true,

		plugins = {
			telescope = true,
		},
	})
	vim.cmd([[colorscheme tokyonight]])
end

-- its all a DREEEEEAAAAAAAMMMM
local function cyberdream()
	require("cyberdream").setup({
		transparent = true,
		extensions = {
			telescope = true,
		},
	})
	vim.cmd([[colorscheme cyberdream]])
end

-- much nature very wow
local function everforest()
	require("lualine").setup({ options = { theme = "everforest" } })

	vim.cmd([[colorscheme everforest]])
end

local function switch(type)
	local cases = {
		["tokiyonight"] = tokiyonight,
		["cyberdream"] = cyberdream,
		["everforest"] = everforest,
		default = everforest,
	}

	local case = cases[type] or cases["default"]
	case()
end

switch(color_scheme)
