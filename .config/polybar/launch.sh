#!/bin/sh

# Terminate already running bar instances
killall -q -KILL polybar
# If all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

export DEFAULT_NETWORK_INTERFACE=$(ip route | grep '^default' | awk '{print $5}' | head -n1)

# Launch Polybar, using default config location ~/.config/polybar/config
polybar -r example 2>&1 | tee -a /tmp/polybar.log

echo "Polybar launched..."
