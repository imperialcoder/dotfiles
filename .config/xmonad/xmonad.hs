import XMonad
import Data.Ratio
import qualified XMonad.StackSet as W

-- action imports 
import XMonad.Actions.GridSelect
import XMonad.Actions.CopyWindow

-- hook imports 
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.UrgencyHook

-- layout imports 
import XMonad.Layout.Gaps
import XMonad.Layout.Magnifier
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders

-- utility imports 
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Cursor

-- general settings
myModMask = mod4Mask
myTerminal = "kitty"
myStatusBar = "~/.config/polybar/launch.sh"
myFocusFollowMouse = False

-- window settings 
myGapSize = 3
myBorderWidth = 2
myNormalBorderColor = "#282A36"
myFocusedBorderColor = "#BD93F9"

myWorkspaces = ["www", "dev", "chat", "game", "vbox", "media"]

-- startup hook 
myStartupHook = do
    spawn myStatusBar
    setDefaultCursor xC_left_ptr

myKeys =
    -- commands/applications
    [ ("M-<Return>", spawn myTerminal)
    , ("M-q", spawn "xmonad --recompile; xmonad --restart")

    -- layout modifiers
    , ("M-S-v", sendMessage NextLayout)
    , ("M-b", sendMessage ToggleStruts)
    , ("M-g", goToSelected def)
    , ("M-,", sendMessage (IncMasterN 1))
    , ("M-.", sendMessage (IncMasterN (-1)))

    -- window focusing
    , ("M-j", windows W.focusDown)
    , ("M-k", windows W.focusUp)
    , ("M-m", windows W.focusMaster)

    -- window moving
    , ("M-S-<Return>", windows W.swapMaster)
    , ("M-S-j", windows W.swapDown)
    , ("M-S-k", windows W.swapUp)

    -- window modifiers
    , ("M-c", kill)
    , ("M-h", sendMessage Shrink)
    , ("M-l", sendMessage Expand)
    , ("M-t", withFocused $ windows . W.sink)
    , ("M-a", windows copyToAll)
    , ("M-S-c", killAllOtherCopies)
    ]

-- keybindings I wish to exclude
myRemovedKeys = 
    [ "M-p"
    , "M-S-c"
    , "M-<Space>"
    , "M-S-q"
    , "M-<Return>"
    , "M-Tab"
    ]

myLayout = avoidStruts . smartBorders . mindDGaps
  $ tiled ||| Mirror tiled ||| Full ||| threeCol
  where
    threeCol = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    tiled    = Tall nmaster delta ratio
    nmaster  = 1      -- Default number of windows in the master pane
    ratio    = 1/2    -- Default proportion of screen occupied by master pane
    delta    = 3/100  -- Percent of screen to increment by when resizing panes
    mindDGaps = spacing myGapSize . gaps [ (U,myGapSize)
                                         , (D,myGapSize)
                                         , (L,myGapSize)
                                         , (R,myGapSize)
                                         ]

myManageHook = composeAll
  [ (className =? "firefox" <&&> title =? "Picture-in-Picture") --> composeAll
        [ doRectFloat (W.RationalRect (29 % 40) (1 % 17) (37 % 200) (1 % 4))
        , doF copyToAll
        ]
    , className =? "Bitwarden" -->
         doRectFloat (W.RationalRect (5 % 14) (1 % 4) (2 % 7) (1 % 2))
    , className =? "Anki" -->
         doRectFloat (W.RationalRect (3 % 8) (1 % 4) (2 % 8) (1 % 2))
    , className =? "Virt-manager" --> composeAll
        [ doRectFloat (W.RationalRect (5 % 14) (1 % 4) (2 % 7) (1 % 2))
        , doShift (myWorkspaces !! 4)
        , doF (W.greedyView (myWorkspaces !! 4))
        ]
    , className =? "Pavucontrol" -->
         doRectFloat (W.RationalRect (5 % 14) (1 % 4) (2 % 7) (1 % 2))
    , className =? "zoom" --> composeAll
        [ doFloat 
        , doShift "chat"
        , doF (W.greedyView "chat")
        ]
    , className =? "thunderbird" -->
         doShift "chat"
    , className =? "discord" -->
         doShift "chat"
    , className =? "firefox" -->
        doShift "www"
    , className =? "Lutris" --> composeAll
        [ doRectFloat (W.RationalRect (5 % 14) (1 % 4) (2 % 7) (1 % 2))
        , doF (W.greedyView "game")
        , doShift "game"
        ]
  ]

defaults = def
    { terminal = myTerminal
    , focusFollowsMouse = myFocusFollowMouse 
    , modMask = myModMask
    , borderWidth = myBorderWidth
    , normalBorderColor = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor
    , workspaces = myWorkspaces
    , startupHook = myStartupHook
    , layoutHook = myLayout
    , manageHook = myManageHook
    }
    `removeKeysP` myRemovedKeys
    `additionalKeysP` myKeys

main = xmonad . docks . ewmhFullscreen . setEwmhActivateHook doAskUrgent . ewmh $ defaults
