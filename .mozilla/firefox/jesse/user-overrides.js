// disable unified extensions
user_pref("extensions.unifiedExtensions.enabled", false);

// use dark theme
user_pref("ui.systemUsesDarkTheme", true);

// enable userChrome.css styling of firefox
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// S P E E D
//
user_pref("gfx.x11-egl.forced-enabled", true);
user_pref("layers.acceleration.forced-enabled", true);
user_pref("media.ffmpeg.vaapi-drm-display.enabled", true);
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.ffvpx.enabled", false);
user_pref("browser.tabs.remote.force-enable", true);
user_pref("dom.ipc.processCount", 4);

// power optimizations (for the laptop)
user_pref("media.rdd-ffmpeg.enabled", true);
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.navigator.mediadatadecoder_vpx_enabled", true);

// enable compact mode to be selectable
user_pref("browser.compactmode.show", true);

// disable alt menu for more keybindings
user_pref("ui.key.menuAccessKeyFocuses", false);

// disable firefox view
user_pref("browser.tabs.firefox-view", false);

// session restore stickie
user_pref("browser.startup.page", 3);
user_pref("browser.sessionstore.privacy_level", 0);
user_pref("privacy.clearOnShutdown.history", false);
user_pref("privacy.cpd.history", false);

/* override recipe: RFP is not for me ***/
user_pref("privacy.resistFingerprinting", false); // 4501
user_pref("privacy.resistFingerprinting.letterboxing", false); // 4504 [pointless if not using RFP]
user_pref("webgl.disabled", false); // 4520 [mostly pointless if not using RFP]

// allow user to choose file_picker
user_pref("ui.allow_platform_file_picker", true);

// 0103: set newwindow/home page for firefox
user_pref("browser.startup.homepage", "about:home");

// 0104: enable newtab page
user_pref("browser.newtabpage.enabled", true);
user_pref("browser.newtab.preload", true);

// 0360: enable Captive Portal detection
//user_pref("captivedetect.canonicalURL", "http://detectportal.firefox.com/canonical.html");
//user_pref("network.captive-portal-service.enabled", true);

// 0403: disable SB checks for downloads (remote)
user_pref("browser.safebrowsing.downloads.remote.enabled", true);

// 2022: use DRM (aka netflix + hulu)
user_pref("media.eme.enabled", true);

// 0801: allow searching through url
user_pref("keyword.enabled", true);

// 0701: disable IPV6 - some reason need this for php
user_pref("network.dns.disableIPv6", false);

// 2801: cookie lifetime policy
user_pref("network.cookie.lifetimePolicy", 0);

// 5003: disable firefox password manager
user_pref("signon.rememberSignons", false);

// 1601: control when to send a cross-origin referer
user_pref("network.http.referer.XOriginPolicy", 0);
// 1602: control the amount of cross-origin information to send
user_pref("network.http.referer.XOriginTrimmingPolicy", 0);

// disable firefox mic and video popup
user_pref("privacy.webrtc.legacyGlobalIndicator", false);
user_pref("privacy.webrtc.hideGlobalIndicator", true);

user_perf("browser.download.folderList", 2);
user_perf("browser.download.dir", "~/downloads");
