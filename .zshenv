# test
export WLR_RENDERER_ALLOW_SOFTWARE=1
export LIBSEAT_BACKEND=logind
export QT_STYLE_OVERRIDE=kvantum

# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# .zprofile/.xinit launch options
export USE_XORG=false
export XORG_WM="leftwm"
#export XORG_WM="bspwm"
#export XORG_WM="awesome"
export WAYLAND_WM="Hyprland"
#export WAYLAND_WM="river"

# XORG
export XCURSOR_THEME="Bibata-Modern-Classic"
export XCURSOR_PATH="/usr/share/icons:$XDG_DATA_HOME/icons"
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc

# apps
export TERMINAL="wezterm"
export BROWSER="qutebrowser"

# editor
export EDITOR="nvim"
export VISUAL="$TERMINAL nvim"

# zsh
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export HISTFILE="$ZDOTDIR/.zhistory"
export HISTSIZE=10000
export SAVEHIST=10000
export PLUGINS="$ZDOTDIR/plugins"
export PATH=/var/lib/flatpak/exports/bin:$HOME/.local/bin/:$PATH

# rust
export RUSTUP_INIT_SKIP_PATH_CHECK="yes" 
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
[ -f $CARGO_HOME/env ] && . $CARGO_HOME/env

# go
export GOBIN="$GOPATH/bin"
export PATH="$PATH:$GOBIN"
export GOPATH="$XDG_DATA_HOME/go"

# ruby
export GEM_HOME="$XDG_CONFIG_HOME/gem"
export GEM_SPEC_CACHE="$XDG_CACHE_HOME/gem"
export PATH="$PATH:$GEM_HOME/bin"

# xmonad
export XMONAD_CONFIG_DIR="$HOME/.config/xmonad"
export XMONAD_DATA_DIR="$XDG_DATA_HOME/xmonad"
export XMONAD_CACHE_DIR="$XDG_CACHE_HOME/xmonad"
export CABAL_CONFIG="$XDG_CONFIG_HOME/cabal/config"
export CABAL_DIR="$XDG_DATA_HOME/cabal"
export STACK_ROOT="$XDG_DATA_HOME/stack"

# kubernetes + docker
export KUBECONFIG="$XDG_CONFIG_HOME/kube/config"
export LESSHISTFILE="$XDG_STATE_HOME/less/history"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"

#dotnet
export PATH="$PATH:/home/jesse/.dotnet/tools"

# ansible
export ANSIBLE_HOME="$XDG_DATA_HOME/ansible"
export ANDROID_HOME="$XDG_DATA_HOME/android"

# npm
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"

# pnpm
export PNPM_HOME="/home/jesse/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"

# pyenv
export PYENV_ROOT="$XDG_DATA_HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
[[ -d $PYENV_ROOT/bin ]] && eval "$(pyenv init -)"

# python venv
export PATH="$PATH:$XDG_DATA_HOME/venv/bin"

# (MACOS SPECIFIC OVERRIDES)
if [[ $(uname) == "Darwin" ]]; then
    export PATH="/opt/homebrew/bin:$PATH" >> ~/.zshrc
    export BROWSER="open"
    [[ -x $(command -v "colima") ]] \
        && export DOCKER_HOST=unix:///$HOME/.colima/docker.sock
fi

# misc 
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export DVDCSS_CACHE="$XDG_DATA_HOME/dvdcss"
export MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export SSB_HOME="$XDG_DATA_HOME/zoom"
export QT_QPA_PLATFORMTHEME=qt5ct
export GRIM_DEFAULT_DIR="$HOME/pictures/screenshots/"
export ANDROID_HOME="$XDG_DATA_HOME/android"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export KDEHOME="$XDG_CONFIG_HOME/kde"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel 
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle
export W3M_DIR="$XDG_DATA_HOME"/w3m
export PYTHONSTARTUP="/etc/python/pythonrc"
export LF_ICONS=$(/bin/cat ~/.config/lf/ICONS)
