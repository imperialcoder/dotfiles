ROOT_DIR := $(shell pwd)
SHARE_DIR := /usr/share/xsessions
TARGET_DIR := /usr/local/bin
LOCAL_DIR := $(HOME)/.local

all: makedirs scripts xdg-ninja leftwm leftwm-theme tt \
	 picom swww tmuxinator rofi-emoji ludusavi zola mdt \
	 frogmouth yazi

makedirs: .FORCE
	@mkdir -p $(LOCAL_DIR)/bin
	@sudo mkdir -p $(SHARE_DIR)

scripts: .FORCE
	ln -sf $(ROOT_DIR)/scripts/* $(LOCAL_DIR)/bin

xdg-ninja: .FORCE
	@echo "linking xdg-ninja"
	ln -sf $(ROOT_DIR)/xdg-ninja/xdg-ninja.sh $(LOCAL_DIR)/bin/xdg-ninja

leftwm: .FORCE
	@echo "building leftwm"
	cd leftwm && git pull origin main && cargo build --profile optimized --no-default-features --features=lefthk,sys-log
	@echo "linking leftwm"
	sudo ln -sf $(ROOT_DIR)/leftwm/target/optimized/leftwm $(TARGET_DIR)/leftwm
	sudo ln -sf $(ROOT_DIR)/leftwm/target/optimized/lefthk-worker $(TARGET_DIR)/lefthk-worker
	sudo ln -sf $(ROOT_DIR)/leftwm/target/optimized/leftwm-worker $(TARGET_DIR)/leftwm-worker
	sudo ln -sf $(ROOT_DIR)/leftwm/target/optimized/leftwm-state $(TARGET_DIR)/leftwm-state
	sudo ln -sf $(ROOT_DIR)/leftwm/target/optimized/leftwm-check $(TARGET_DIR)/leftwm-check
	sudo ln -sf $(ROOT_DIR)/leftwm/target/optimized/leftwm-command $(TARGET_DIR)/leftwm-command
	sudo ln -sf $(ROOT_DIR)/leftwm/leftwm.desktop $(SHARE_DIR)/leftwm.desktop

leftwm-theme: .FORCE
	@echo "building leftwm-theme"
	cd leftwm-theme && \
		git pull origin master && \
		cargo build --release
	@echo "linking leftwm-theme"
	ln -sf $(ROOT_DIR)/leftwm-theme/target/release/leftwm-theme $(LOCAL_DIR)/bin

tt: .FORCE
	@echo "building tt"
	cd tt && make
	@echo "linking tt"
	ln -sf $(ROOT_DIR)/tt/bin/tt $(LOCAL_DIR)/bin

picom: .FORCE
	@echo "building picom"
	cd picom && \
		meson --buildtype=release . build && \
		ninja -C build
	@echo "linking picom"
	sudo ln -sf $(ROOT_DIR)/picom/build/src/picom $(TARGET_DIR)/picom

swww: .FORCE
	@echo "building swww"
	cd swww && cargo build --release
	ln -sf $(ROOT_DIR)/swww/target/release/swww $(LOCAL_DIR)/bin
	ln -sf $(ROOT_DIR)/swww/target/release/swww-daemon $(LOCAL_DIR)/bin

tmuxinator: .FORCE
	@echo "building tmuxinator"
	gem update
	cd tmuxinator && bundler install
	ln -sf $(ROOT_DIR)/tmuxinator/bin/tmuxinator $(LOCAL_DIR)/bin

rofi-emoji: .FORCE
	@echo "building rofi-emoji"
	cd rofi-emoji && \
		autoreconf -i && \
		mkdir -p build && cd build && ../configure && \
		make && sudo make install
	libtool --finish /usr/lib64/rofi/

ludusavi: .FORCE
	@echo "building ludusavi"
	cd ludusavi && cargo build --release
	ln -sf $(ROOT_DIR)/ludusavi/target/release/ludusavi $(LOCAL_DIR)/bin

zola: .FORCE
	@echo "building zola"
	cd zola && cargo build --release
	ln -sf $(ROOT_DIR)/zola/target/release/zola $(LOCAL_DIR)/bin

mdt: .FORCE
	@echo "building mdt"
	go install github.com/charmbracelet/gum@latest
	ln -sf $(ROOT_DIR)/mdt/mdt $(LOCAL_DIR)/bin

frogmouth: .FORCE
	@echo "building frogmouth"
	python3 -m venv $(ROOT_DIR)/frogmouth
	sleep 1 && \
		source $(ROOT_DIR)/frogmouth/bin/activate && \
		pip install frogmouth
	ln -sf $(ROOT_DIR)/frogmouth/bin/frogmouth $(LOCAL_DIR)/bin

yazi: .FORCE
	@echo "building yazi"
	cargo build --manifest-path yazi/Cargo.toml --release
	ln -sf $(ROOT_DIR)/yazi/target/release/yazi $(LOCAL_DIR)/bin

newsboat: .FORCE
	@echo "building newsboat"
	make -C newsboat
	sudo make -C newsboat install

neonmodem: .FORCE
	@echo "building neonmodem"
	make -C neonmodem
	ln -sf $(ROOT_DIR)/neonmodem/neonmodem $(LOCAL_DIR)/bin

eww: .FORCE
	@echo "building eww"
	cd eww && cargo build --release --no-default-features --features=wayland
	ln -sf $(ROOT_DIR)/eww/target/release/eww $(LOCAL_DIR)/bin

swhkd: .FORCE
	@echo "building swhkd"
	make -C swhkd setup
	make -C swhkd build
	sudo make -C swhkd install

photon: .FORCE
	@echo "building photon"
	make -C photon
	sudo make -C photon install

ratt: .FORCE
	@echo "building ratt"
	sudo make -C ratt install

stmp: .FORCE
	@echo "building stmp"
	cd stmp && go build
	ln -sf $(ROOT_DIR)/stmp/stmp $(LOCAL_DIR)/bin

jellycli: .FORCE
	@echo "bulding jellycli"
	cd jellycli && go build
	ln -sf $(ROOT_DIR)/jellycli/jellycli $(LOCAL_DIR)/bin

twitch-tui: .FORCE
	@echo "building twitch-tui"
	cd twitch-tui && cargo build --release
	ln -sf $(ROOT_DIR)/twitch-tui/target/release/twt $(LOCAL_DIR)/bin

wezterm: .FORCE
	@echo "building wezterm"
	cd wezterm && cargo build --release
	ln -sf $(ROOT_DIR)/wezterm/target/release/wezterm $(LOCAL_DIR)/bin

sherlock:
	@echo "building sherlock"
.FORCE:
