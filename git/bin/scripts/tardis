#!/bin/sh
base_mount="/mnt/tardis"
snap_subvol="@snapshots"
snap_buffer="1 week ago"

readarray -t btdev < <(sudo btrfs filesystem show -m | awk '/ path /{print $NF}')

function findirs () {
    find $1 -mindepth 1 -maxdepth 1 ${@:2} -type d
}

function findsubs () {
    findirs ${@} -not -name $snap_subvol 
}

function mount_drives () {

    for drive in ${btdev[@]}; do
        local target="${base_mount}/$(basename $drive)"
        sudo mount -m "$drive" "$target"
        [[ $1 == '-l' || $1 == '--log' ]] && \
            echo "mounting ${drive} from ${target}"
    done
    [[ $1 == '-l' || $1 == '--log' ]] && \
        echo "done!"
}

function umount_drives () {
    for drive in ${btdev[@]}; do
        local target="${base_mount}/$(basename $drive)"
        sudo umount "$target"
        [[ $1 == '-l' || $1 == '--log' ]] && \
            echo "unmounting ${drive} from ${target}"
    done
    sudo rm -rf $base_mount
    [[ $1 == '-l' || $1 == '--log' ]] && \
        echo "done!"
}

function clean_snapshots () {
    snap_mount=$1

    for snapshot in $(findirs $snap_mount -mindepth 2 -maxdepth 2); do
        date_string=$(echo $snapshot | rev | cut -d / -f1 | rev | tr '_' ' ')

        if date --date "$date_string" &> /dev/null; then
            buffer_date=$(date --date "$snap_buffer" +%s)
            target_date=$(date --date "$date_string" +%s)

            if [ $buffer_date -gt $target_date ]; then
                echo "deleting $(echo $snapshot | rev | cut -d / -f1-4 | rev)"
                sudo btrfs subvolume delete $snapshot 1> /dev/null
            fi
        fi
    done
}

function cleanup () {
    mount_drives
    for drive_mount in $(findirs $base_mount); do
        clean_snapshots "${drive_mount}/${snap_subvol}"
    done
    umount_drives
}

function restore () {
    mount_drives

    selected_base=$(findirs "$base_mount/*/$snap_subvol" \
        | fzf --header "select subvolume" --delimiter / --with-nth=-3,-1)
    selected_target=$(echo $selected_base | sed "s|/$snap_subvol||g")
    selected_snapshot=$(findirs $selected_base \
        | fzf --header "select snapshot" --delimiter / --with-nth=-1)

    echo $selected_target $selected_snapshot

    umount_drives
}

function snapshot () {
    mount_drives
    snap_date=$(date +%Y-%m-%d_%H:%M:%S)

    for drive_mount in $(findirs $base_mount); do
        snap_mount="${drive_mount}/${snap_subvol}"

        [ ! -d $snap_mount ] && continue

        clean_snapshots $snap_mount
        for subvol in $(findsubs $drive_mount); do
            echo "creating $(basename $drive_mount)/${snap_subvol}/$(basename $subvol)"
            sudo mkdir -p $snap_mount/$(basename $subvol)
            sudo btrfs subvolume snapshot -r \
                $subvol $snap_mount/$(basename $subvol)/$snap_date \
                1> /dev/null
        done
    done

    umount_drives
    echo "done!"
}

case $1 in
    mount) mount_drives -l ${@:2} ;;
    umount) umount_drives -l ${@:2} ;;
    cleanup) cleanup ${@:2} ;;
    restore) restore ${@:2} ;;
    snapshot) snapshot ${@:2} ;;
esac
